import $ from 'jquery'

(() => {
    let oldLog = console.log;
    console.log = function (message) {
        $('#console').append(message + "\r\n");
        oldLog.apply(console, arguments);
    };
})();

let currentUser = document.getElementById('from');
let targetUser = document.getElementById('to');

if (localStorage.getItem('from')) {
    currentUser.value = localStorage.getItem('from');
}
if (localStorage.getItem('to')) {
    targetUser.value = localStorage.getItem('to');
}


document.getElementById('init-app').addEventListener('click', initApp);

if (localStorage.getItem('from') && localStorage.getItem('to')) {
    initApp();
}

function initApp() {
    $('#p2p-block').show();
    $('#init-block').hide();
    currentUser = currentUser.value;
    targetUser = targetUser.value;
    localStorage.setItem('from', currentUser);
    localStorage.setItem('to', targetUser);

    if (!currentUser || !targetUser) {
        alert('currentUser or targetUser is undefined');
        return;
    }

    console.log(`from: ${currentUser}, to: ${targetUser}`);

    let config = {
        iceServers: [
            {urls: 'stun:stun.l.google.com:19302'},
            {urls: 'stun:global.stun.twilio.com:3478?transport=tcp'},
            {
                urls: "turn:165.227.154.9:3478",
                username: "tallium",
                credential: "RcF4zKPHH9UZMR"
            }
        ],
        iceTransportPolicy: 'relay'
    };

    let connection = new RTCPeerConnection(config);
    let dataChannel;

    function sendToServer(msg) {
        socket.send(JSON.stringify(msg));
    }

    function handleNewICECandidateMsg(msg) {
        // console.log('handleNewICECandidateMsg', msg);
        let candidate = new RTCIceCandidate(msg.candidate);
        connection.addIceCandidate(candidate).catch(() => {
            console.info('Candidate already added');
        });
    }

    function handleOfferMsg(msg) {
        connection.setRemoteDescription(msg.sdp);

        connection.createAnswer()
            .then((event) => {
                connection.setLocalDescription(event);
            })
            .then(() => {
                sendToServer({
                    type: "answer",
                    from: currentUser,
                    to: targetUser,
                    sdp: connection.localDescription
                });
            });

        initDataChannel('answer');
    }

    function handleAnswerMsg(msg) {
        connection.setRemoteDescription(msg.sdp);
    }

    //let socket = io('https://turn-stage.tallium.com:8888');
    //let socket = new WebSocket('ws://localhost:8899', 'signal-server');
    let socket = new WebSocket('wss://turn-stage.tallium.com:8899', 'signal-server');
    socket.onopen = () => {
        console.log('connection opened');
        socket.send(JSON.stringify({
                type: 'join',
                data: currentUser
            }));
        setTimeout(() => {
            init();
        }, 1000);
    };

    socket.onmessage = socketEventHandler;

    function socketEventHandler(data) {
        data = JSON.parse(data.data);
        //console.log('socket', data);
        switch (data.type) {
            case 'new-ice-candidate':
                console.log("ws new-ice-candidate");
                handleNewICECandidateMsg(data);
                break;
            case 'offer':
                console.log("ws offer");
                handleOfferMsg(data);
                break;
            case 'answer':
                console.log("ws answer");
                handleAnswerMsg(data);
                break;
        }
    }

    connection.onconnectionstatechange = () => {
        switch(connection.connectionState) {
            case "connected":
                console.log(`[connected]`);
                break;
            case "disconnected":
                console.log(`[disconnected]`);
                // TODO reconnect
                break;
            case "failed":
                console.log(`[failed]`);
                break;
            case "closed":
                console.log(`[closed]`);
                break;
        }
    };

    connection.onicecandidate = (event) =>  {
        console.log('onicecandidate', event);
        if (event.candidate) {
            console.log("onicecandidate new-ice-candidate", event.candidate);
            socket.send(JSON.stringify({
                type: "new-ice-candidate",
                to: targetUser,
                from: currentUser,
                candidate: event.candidate
            }));
        } else if (connection.remoteDescription === null) {
            console.log('onicecandidate offer', connection.localDescription);
            sendToServer({
                type: "offer",
                from: currentUser,
                to: targetUser,
                sdp: connection.localDescription
            });
        }
    }

    let initDataChannel = (type) => {
        if (typeof dataChannel !== 'undefined') {
            return;
        }

        switch (type) {
            case 'offer':
                dataChannel = connection.createDataChannel("chat");
                dataChannel.onopen = () => {
                    addMessage('connected');
                };
                dataChannel.onmessage = (event) => {
                    addMessage(event.data);
                };
                break;

            case 'answer':
                connection.ondatachannel = (event) => {
                    dataChannel = event.channel;
                    dataChannel.onopen = () => {
                        addMessage('connected');
                    };
                    dataChannel.onmessage = (event) => {
                        addMessage(event.data);
                    };
                }
        }
    };

    $('#start').click(init);

    function init() {
        initDataChannel('offer');

        connection.createOffer().then((event) => {
            console.log('---', event);
            connection.setLocalDescription(event);
        });
    }

    let addMessage = (message) => {
        $('#chat').append(message + "\r\n");
    };

    $('#send').click(() => {
        let data = $('#chat-input').val();
        console.log(connection);

        if (dataChannel) {
            $('#chat-input').val('');

            dataChannel.send(data);

            addMessage(data);
        } else {
            console.error("Can't send message, dataChannel is undefined");
        }
    });
}